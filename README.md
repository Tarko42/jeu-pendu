Le pendu est un jeu consistant à trouver un mot en devinant quelles sont les lettres qui le composent. Le jeu se joue traditionnellement à deux, avec un papier et un crayon, selon un déroulement bien particulier. Plus récemment, il peut aussi se jouer en ligne contre un ordinateur.

Il est occasionnellement pratiqué dans les salles de classe où un élève au tableau joue contre l'ensemble de ses camarades, en dessinant à la craie ou au feutre, mémorisant ainsi du vocabulaire d'une manière ludique tout en corsant plus ou moins la difficulté du jeu.

Le dessin une fois terminé montre une scène d'exécution par pendaison dans laquelle un bonhomme allumette représente le pendu. 

source : Wikipédia
