// Phase d'import des différentes classes
import Clavier from "./Clavier.js"
import NewPartie from "./NewPartie.js"

// Création des instances de classes
const clavier = new Clavier('clavier');
const nouvellePartie = new NewPartie();

// Appel des méthodes
clavier.creerClavier();


document.getElementById('sauvegarderMot').addEventListener('click', ()=>{
    const motMystereValue = document.getElementById('input').value;
    console.log(motMystereValue);
    clavier.enregistrerMotmystere(motMystereValue);
})

