// classe qui gère le clavier

export default class Clavier {

    motMystere = "";
    arrayMystere = [];
    lettreValide = [];
    essaisRestants = 7;

    constructor(containerID, lettre) {
        this.containerID = containerID;
        this.lettre = lettre;
    }

    // méthode permettant de créer le clavier
    creerClavier() {

        const container = document.getElementById(this.containerID);
        // charCodeAt est une méthode qui permet de récupérer le numéro unicode d'un caractère
        const alphabet = 'A'.charCodeAt(0);

        // Boucle pour créer des boutons pour chaque lettre de l'alphabet
        for (let i = 0; i < 26; i++) {
            const lettre = String.fromCharCode(alphabet + i);
            const bouton = document.createElement('button');
            bouton.textContent = lettre;
            bouton.addEventListener('click', () => this.lettreSelectionnee(lettre));
            container.appendChild(bouton);
        }
    }
    // méthode pour la manipulation des lettres et la comparaison avec le mot mystère
    lettreSelectionnee(lettre) {
        console.log(`Lettre sélectionnée : ${lettre}`);
        this.comparerLettre(lettre);
    }

    enregistrerMotmystere(motMystereParam) {
        this.motMystere = motMystereParam.toLowerCase();
        for (let i = 0; i < this.motMystere.length; ++i) {
            const lettreEnCours = this.motMystere[i];
            // "!" sert à nier la proposition, si arraymystere n'est pas inclut dans lettreencours alors:
            if (!this.arrayMystere.includes(lettreEnCours)) {
                this.arrayMystere.push(lettreEnCours);
            }

        }
        console.log(this.arrayMystere);
        this.displayWord(this.motMystere, this.lettreValide)

    }

    
    comparerLettre(lettre) {
        const lettreSelectionnee = lettre.toLowerCase();
        let lettreTrouvee = false;
        

        for (let i = 0; i < this.motMystere.length; i++) {
            if (lettreSelectionnee === this.motMystere[i]) {
                console.log(`Vous avez trouvé la lettre "${lettreSelectionnee}" à la position ${i + 1}.`);
                this.lettreValide.push(lettreSelectionnee);
                lettreTrouvee = true;
            }
            console.log(this.lettreValide);
        }

        if (!lettreTrouvee) {
            console.log(`La lettre "${lettreSelectionnee}" n'est pas présente dans le mot.`);
            this.essaisRestants--;

            if (this.essaisRestants === 0) {
                console.log("Vous n'avez plus d'essais. La partie est terminée.");
                document.querySelector("#finPartieDefaite").style.display = "block";
            } else {
                console.log(`Il vous reste ${this.essaisRestants} essais.`);
                erreurUtilisateur(); // Appel de la fonction pour l'affichage du dessin de pendu
            }
        }
        // every est une méthode spécifique au tableau, elle renvoie un booléen en testant une condition sur tous les élements du tableau, j'utilise également array pour convertir ma chaine de caractère en tableau, ce qui me permet d'appliquer la méthode désirée
        const toutesLesLettresTrouvees = Array.from(this.motMystere).every(lettreMot => this.lettreValide.includes(lettreMot));

        this.displayWord(this.motMystere, this.lettreValide)

        if (toutesLesLettresTrouvees) {
            console.log("Félicitations ! Vous avez trouvé toutes les lettres. La partie est terminée.");
            document.querySelector("#finPartie").style.display = "block";
        }
    }

    displayWord(motMystere, lettreValide) {
        const resultat = motMystere.split('').map(char => lettreValide.includes(char) ? char : '_ ').join('');
        console.log(resultat);
        document.querySelector("#suivieMot").innerHTML = `<p id=progression> ${resultat}</p>`;
        return resultat;

    }

}
