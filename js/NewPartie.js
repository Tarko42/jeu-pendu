// classe qui s'occupe de lancer la pop up et de configurer une nouvelle partie

export default class NewPartie {
    
    nouvellePartieBoutton;

    constructor() {
        this.nouvellePartieBoutton = document.getElementById('newGame');
        this.nouvellePartieBoutton.addEventListener('click', () => this.ouvrirPopUp());
    }

    ouvrirPopUp() {
        const divPopup = document.getElementById('popup')
        divPopup.style.display = "block"
    }
}


