// Prog procédurale

function fermerPopup() {
    let fermerPopup = document.getElementById('popup');
    fermerPopup.style.display = "none";
}

// Affichage du dessin de pendu en fonction des erreurs

const divPotence = document.getElementById('potence');
const penduImage = document.createElement('img');
penduImage.id = 'potenceImg'

const imagesPendu = [
    '../medias/potence-0.png',
    '../medias/potence-1.png',
    '../medias/potence-2.png',
    '../medias/potence-3.png',
    '../medias/potence-4.png',
    '../medias/potence-5.png',
    '../medias/potence-6.png',
    
];

let nombreErreurs = 0;

function afficherEtapePendu() {
    const indexImage = Math.min(nombreErreurs, imagesPendu.length - 1);

    penduImage.src = imagesPendu[indexImage];

    divPotence.innerHTML = '';
    divPotence.appendChild(penduImage);
}

function erreurUtilisateur() {
    nombreErreurs++;
    afficherEtapePendu();
}

afficherEtapePendu();


