# 1. 
Afficher dans la console le mot pour le joueur : "TARKO" avec 'A' et 'O' trouvés => afficher "_A__O"
# 2. 
créer une condition de victoire, et afficher un message de victoire lorsqu'elle est remplie
# 3. 
Ajouter un nombre maximal de coups, et un message de défaite s'il est atteint et qu'il reste des lettres à trouver
# 4. 
Afficher tout ça à l'écran et non dans la console, en mettant à jour les informations après chaque coup (mot avec lettres trouvées,
nombre de coups restants)
# 5. 
Dessiner le pendu à l'écran au fur et à mesure des coups joués (par exemple en ayant le dessin sur la page en display:none 
et en rendant visible une nouvelle partie à chaque coup

